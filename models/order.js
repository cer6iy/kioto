var mongoose = require('lib/mongoose'),
    Schema = mongoose.Schema;

var order = Schema({
    date: {
        type: Date,
        default: Date.now(),
        required: true
    },
    basket: [{
        _id: false,
        product: {
            type: Schema.Types.ObjectId,
            ref: 'product',
            required: true
        },
        count: {
            type: Number
        },
        sum: {
            type: Number
        }
    }],
    client: {
        name: {
            type: String,
            required: true
        },
        phone: {
            type: String,
            required: true
        }
    },
    delivery: {            // доставка
        address: {
            street: {
                type: String
            },
            building: {
                type: String
            },
            padik: { //?
                type: Number
            },
            flat: {
                type: Number
            },
            house: {
                type: Number
            },
            porch: {
                type: Number
            },
            doorphone: {
                type: Number
            }
        }
    },
    selfExport: {               // самовывоз
        shop: {
            type: Schema.Types.ObjectId,
            ref: 'shop'
        }
    },
    time: {
        type: Number
    },
    type: {                 // delivery || selfExport
        type: String,
        required: true
    },
    sum: {                  // total sum
        type: Number,
        required: true
    },
    processed: {
        type: Boolean,
        default: false
    },
    processedDate: {
        type: Date
    },
    active: {
        type: Number,
        required: true,
        default: 1
    }
});

module.exports = mongoose.model('order', order);