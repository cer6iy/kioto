var mongoose = require('lib/mongoose'),
    Schema = mongoose.Schema;

var product = Schema({
    name: {
        type: String,
        required: true
    },
    ingredients: [{
        text: {type: String}
    }],
    cost: {
        type: Number,
        required: true
    },
    old_cost: {
        type: Number
    },
    weight: {
        type: Number,
        required: true
    },
    sort: {
        type: Number,
        default: 999
    },
    type: {
        type: String,
        required: true,
        default: "roll"
    }
});

module.exports = mongoose.model('product', product);