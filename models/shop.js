var mongoose = require('lib/mongoose'),
    Schema = mongoose.Schema;

var shop = Schema({
    name: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    workFrom: {
        type: Number,
        required: true,
        default: 9
    },
    workTo: {
        type: Number,
        required: true,
        default: 21
    },
    phone: {
        type: String,
        required: true
    },
    lat: {
        type: Number
    },
    lon: {
        type: Number
    }
});

module.exports = mongoose.model('shop', shop);