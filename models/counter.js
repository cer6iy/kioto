var mongoose = require('lib/mongoose'),
    Schema = mongoose.Schema;

var Counter = mongoose.model('counters', Schema({
        _id: {
            type: String,
            required: true
        },
        sequence: {
            type: Number,
            required: true
        }
    }, {
        collection: 'counters',
        versionKey: false
    })
);

var getNext = function (collection, next) {
    if ('function' === typeof collection){
        next = collection;
        collection = 'common';
    }
    var query = {
        _id: collection
    };
    var update = {
        $inc: {
            sequence: 1
        }
    };
    var options = {
        upsert: true
    };
    Counter.findOneAndUpdate(query, update, options, function (err, counter) {
        if (err)
            console.log('Error from counter.js: ' + err);

        next(counter.sequence);
    });
}

module.exports.getNext = module.exports.next = getNext;