var mongoose = require('lib/mongoose'),
    Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');

var userSchema = Schema({
    firstName: {
        type: String
    },
    lastName: {
        type: String
    },
    userName: {
        type: String,
        required: true,
        unique: true
    },
    birthday: {
        type: Date
    },
    aboutText: {
        type: String
    },
    phone: {
        type: String
    },
    role: {
        type: String,
        default: 'user'
    },
    local: {
        password: String
    },
    admin: {
        type: Boolean,
        required: true,
         default: false
    }
});

// generating a hash
userSchema.methods.generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.local.password);
};

module.exports = mongoose.model('user', userSchema);