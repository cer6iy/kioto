var request = require('request');

var Shop = require('models/shop');

var MAX_LIMIT = 100;

module.exports.all = function (req, res, next) {
    var limit = (!req.query.limit || req.query.limit > MAX_LIMIT) ? MAX_LIMIT : req.query.limit,
        skip = req.query.skip || 0,
        sortField = req.query.sort || '-name',
        query = (req.query.q) ? JSON.parse(req.query.q) : {},
        fieldsToSelect = req.query.fields || '';

    Shop
        .find(query)
        .sort(sortField)
        .select(fieldsToSelect)
        .skip(skip)
        .limit(limit)
        .exec(function (err, data) {
            if (err) return next(err);
            if (data && data.length != 0) {
                res.json({
                    status: 200,
                    data: data
                });
            } else {
                var error = new Error("Shops not found");
                error.status = 404;
                next(error);
            }
        })
};

module.exports.byId = function (req, res, next) {
    Shop.findById(req.params.id, function (err, s) {
        if (err) return next(err);
        res.json({
            status: 200,
            data: s
        });
    })
};

module.exports.new = function (req, res, next) {
    console.log(req.body);
    var shop = new Shop(req.body);
    shop.save(function (err, s) {
        if (err) return next(err);
        res.json({
            status: 200,
            data: s
        });
    })
};

module.exports.update = function (req, res, next) {
    Shop.findByIdAndUpdate({_id: req.params.id}, {$set: req.body}, function (err, s) {
        if (err) return next(err);
        res.json({
            status: 200,
            data: s
        });
    })
};

module.exports.delete = function (req, res, next) {
    Shop.remove({_id: req.params.id}, function (err, s) {
        if (err) return next(err);
        res.json({
            status: 200,
            data: s
        });
    })
};
