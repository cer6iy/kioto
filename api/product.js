var Product = require('models/product');
var fs = require('fs');

var MAX_LIMIT = 100;

module.exports.all = function (req, res, next) {
    var limit = (!req.query.limit || req.query.limit > MAX_LIMIT) ? MAX_LIMIT : req.query.limit,
        skip = req.query.skip || 0,
        sortField =  'sort',
        query = (req.query.q) ? JSON.parse(req.query.q) : {},
        fieldsToSelect = req.query.fields || '';

    Product
        .find(query)
        .sort(sortField)
        .select(fieldsToSelect)
        .skip(skip)
        .limit(limit)
        .exec(function (err, data) {
            if (err) return next(err);
            if (data && data.length != 0) {
                res.json({
                    status: 200,
                    data: data
                });
            } else {
                console.log(data);
                var error = new Error("Products not found");
                error.status = 404;
                next(error);
            }
        })
};

module.exports.byId = function (req, res, next) {
    Product.findById(req.params.id, function (err, s) {
        if (err) return next(err);
        res.json({
            status: 200,
            data: s
        });
    })
};

module.exports.new = function (req, res, next) {
    console.log(req.body);
    var product = new Product(req.body);
    product.save(function (err, s) {
        if (err) return next(err);
        res.json({
            status: 200,
            data: s
        });
    })
};

module.exports.update = function (req, res, next) {
    Product.findByIdAndUpdate({_id: req.params.id}, {$set: req.body}, function (err, s) {
        if (err) return next(err);
        res.json({
            status: 200,
            data: s
        });
    })
};

module.exports.delete = function (req, res, next) {
    Product.remove({_id: req.params.id}, function (err, s) {
        if (err) return next(err);
        res.json({
            status: 200,
            data: s
        });
    })
};
