var express = require('express');
var router = express.Router();

var mag = require('mag');
var l = mag('api');

var adminAccess = require('lib/passport').helpers.adminAccess;
var adminLogined = require('lib/passport').helpers.adminLogined;

var auth = require('./auth');
var product = require('./product');
var order = require('./order');
var shop = require('./shop');

/* auth */

router.post('/signup', auth.signup);
router.post('/login', auth.login);
router.get('/logout', auth.logout);

/* product */

router.get('/products', product.all);
router.get('/products/:id', product.byId);
router.post('/products', adminAccess, product.new);
router.post('/products/:id', adminAccess, product.update);
router.delete('/products/:id', adminAccess, product.delete);

/* order */

router.get('/orders', order.all);
router.get('/orders/:id', order.byId);
router.post('/orders', order.new);
router.post('/orders/:id', adminAccess, order.update);
router.delete('/orders/:id', adminAccess, order.delete);

/* shop */

router.get('/shops', shop.all);
router.get('/shops/:id', shop.byId);
router.post('/shops', adminAccess, shop.new);
router.post('/shops/:id', adminAccess, shop.update);
router.delete('/shops/:id', adminAccess, shop.delete);


router.use(function(req, res, next) {
    var err = new Error('Path Not Found');
    err.status = 404;
    next(err);
});
/*router.use(function(err, req, res, next) {
    l.error('path: ' + req.path + ' error: ' + err.message);
    res.status(err.status || 500)
        .send({
            status: err.status || 500,
            message: err.message,
            error: err
        });
});*/


module.exports = router;
