var passport = require('passport');

module.exports.login = function (req, res, next) {
    console.log(req.body);
    passport.authenticate('local-login', function (err, user, info) {
        if (err && err.code === 409) return next(err);
        if (err) return next(err);
        req.logIn(user, function (err) {
            if (err) return next(err);
            res.send(user);
        });
    })(req, res, next);
};

module.exports.signup = function (req, res, next) {
    passport.authenticate('local-signup', function (err, user, info) {
        if (err && err.code === 409) return res.send(err.code, err);
        if (err) return next(err);
        req.login(user, function (err) {
            if (err) return next(err);
            return res.send(user);
        });
    })(req, res, next);
};

module.exports.logout = function (req, res, next) {
    req.logout();
    res.redirect('/admin');
};

module.exports.helpers = {
    authorizedAccess: function(req, res, next){

    }
};