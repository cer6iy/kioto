var Order = require('models/order');
var Product = require('models/product');
var EventEmitter = require('events').EventEmitter;

var emitter = new EventEmitter();

var MAX_LIMIT = 30;

module.exports.all = function (req, res, next) {
    var limit = (!req.query.limit || req.query.limit > MAX_LIMIT) ? MAX_LIMIT : req.query.limit,
        skip = req.query.skip || 0,
        sortField = req.query.sort || '-date',
        query = (req.query.q) ? JSON.parse(req.query.q) : {},
        fieldsToSelect = req.query.fields || '',
        populateOptions = [{path: 'basket.product'}, {path: 'selfExport.shop'}];

    query.processed = (query.processed === 'undefined' || query.processed === null) ? false : query.processed;
    query.active = 1;
    console.log(query)
    if (req.query.mode === 'count') {
        Order.count(query, function (err, data) {
            if (err) return next(err);
            res.json({
                status: 200,
                data: data
            });
        })
    } else {
        Order
            .find(query)
            .sort(sortField)
            .select(fieldsToSelect)
            .skip(skip)
            .limit(limit)
            .populate(populateOptions)
            .exec(function (err, data) {
                if (err) return next(err);
                if (data) {
                    res.json({
                        status: 200,
                        data: data
                    });
                } else {
                    console.log(data);
                    var error = new Error("Orders not found");
                    error.status = 404;
                    next(error);
                }
            })
    }
};

module.exports.byId = function (req, res, next) {
    Order.findById(req.params.id, function (err, s) {
        if (err) return next(err);
        res.json({
            status: 200,
            data: s
        });
    })
};

module.exports.new = function (req, res, next) {
    console.log(req.body);
    var orderBody = req.body;
    var productIds = [];
    orderBody.sum = 0;
    for (var i = 0; i < orderBody.basket.length; i++) {
        productIds.push(orderBody.basket[i].product);
    }
    Product.find({_id: {$in: productIds}}, function (err, products) {
        if (err) return next(err);
        if (!products || products.length == 0) {
            return next(new Error("Products not found. Nothing to buy."))
        }
        for (var i = 0; i < orderBody.basket.length; i++) {
            var pos = -1;
            for (var j = 0; j < products.length; j++) {
                if (orderBody.basket[i].product.toString() === products[i]._id.toString()) {
                    pos = j;
                    break;
                }
            }
            orderBody.basket[i].sum = orderBody.basket[i].count * products[i].cost;
            orderBody.sum += orderBody.basket[i].sum;
        }
        var order = new Order(orderBody);
        order.save(function (err, s) {
            if (err) return next(err);
            res.json({
                status: 200,
                data: s
            });
            emitter.emit('newOrder', s);
        });
    });


};

module.exports.update = function (req, res, next) {
    delete req.body.basket;
    delete req.body.date;
    console.log(req.body)
    Order.findByIdAndUpdate({_id: req.params.id}, {$set: req.body}, function (err, s) {
        if (err) return next(err);
        res.json({
            status: 200,
            data: s
        });
    })
};

module.exports.delete = function (req, res, next) {
    //Order.remove({_id: req.params.id}, function (err, s) {
    //    if (err) return next(err);
    //    res.json({
    //        status: 200,
    //        data: s
    //    });
    //})

    Order.update({_id: req.params.id}, {$set: {active: 0}}, function (err, s) {
        if (err) return next(err);
        res.json({
            status: 200,
            data: s
        });
    })
};

module.exports.emitter = emitter;