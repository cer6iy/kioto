require('lib/mag-init');
/**
 * Module dependencies.
 */
var l = require('mag')('server');
var app = require('app');
var http = require('http');
var config = require('config');


/**
 * Get port from environment and store in Express.
 */

var port = config.port;
app.set('port', port);

/**
 * Create HTTP server.
 */

var server = http.createServer(app);
var io = require('socket.io')(server);

var orderEmitter = require('api/order').emitter;
io.on('connection', function (socket) {
    var callback = function (data) {
        console.log("###### from emitter #########");
        console.log(data)
        socket.emit('newOrder', data);
    };
    console.log('connected')
    orderEmitter.on('newOrder', callback);
    socket.on('disconnect', function () {
        console.log('disconnected')
        orderEmitter.removeListener('newOrder',callback);
        console.log(orderEmitter.listeners('newOrder').length)
    })
    console.log(orderEmitter.listeners('newOrder').length)
});


/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            l.error('Port ' + port + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            l.error('Port ' + port + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    l.debug('Listening on port ' + server.address().port);
}

