'use strict';
var auth = angular.module('auth-controller', []);
auth.controller('signin', ['$scope', '$window', 'Login', function ($scope, $window, Login) {
    $scope.user = {};
    $scope.saveData = function () {
        $scope.errors = [];
        if (!$scope.user.login) $scope.errors.push({message: 'Login Required'});
        if (!$scope.user.password) $scope.errors.push({message: 'Password Required'});

        if (!$scope.errors || $scope.errors.length === 0) {
            Login.query($scope.user, function (res) {
                console.log(res);
                $window.location.href = '/';
            });
        }
    };
}]);

auth.controller('signup', ['$scope', '$window', 'SignUp', function ($scope, $window, SignUp) {
    $scope.user = {};
    $scope.saveData = function () {
        $scope.errors = [];
        if (!$scope.user.fName) $scope.errors.push({message: 'First Name Required'});
        if (!$scope.user.lName) $scope.errors.push({message: 'Last Name Required'});
        if (!$scope.user.login) $scope.errors.push({message: 'Login Required'});
        if (!$scope.user.password) $scope.errors.push({message: 'Password Required'});

        if (!$scope.errors || $scope.errors.length === 0) {
            SignUp.query($scope.user, function (res) {
                console.log(res);
                $window.location.href = '/users/login';
            });
        }
    };
}]);
