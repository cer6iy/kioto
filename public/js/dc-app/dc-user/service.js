var services = angular.module('auth-services', ['ngResource']);

services.factory('Login', ['$resource',
    function($resource, user){
        return $resource('/api/login', user, {
            query: {method:'POST'}
        });
    }]);