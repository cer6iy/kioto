'use strict';
var service = angular.module('service-controller', ['ngResource', 'ngTagsInput']);


auth.controller('login', ['$scope', '$window', 'Login', function ($scope, $window, Login) {
    $scope.user = {
        login: '',
        password: ''
    };
    $scope.login = function () {
        $scope.errors = [];
        if (!$scope.user.login) $scope.errors.push({message: 'Login Required'});
        if (!$scope.user.password) $scope.errors.push({message: 'Password Required'});

        if (!$scope.errors || $scope.errors.length === 0) {
            Login.query($scope.user, function (res) {
                console.log(res);
                $window.location.href = '/admin';
            });
        }
    };
}]);

auth.controller('mainAdmin', ['$scope', '$resource', function ($scope, $resource) {


    $scope.products = [];
    $scope.ordersCount = 0;

    var Product = $resource('/api/products:id');
    var Order = $resource('/api/orders:id');

    var getOrders = function (next) {
        Order.get({q: "{\"processed\": false}", sort: '-date', mode: "count"}, function (res) {
            $scope.ordersCount = res.data;
            if (typeof next === 'function') next()
        })
    };

    getOrders(function () {
        Product.get({limit: 6, fields: "name"}, function (res) {
            $scope.products = res.data;
        });
    });

    //var socket = io('http://sushimarket-kioto.com/'); // TODO: uncomment to production
    var socket = io('http://localhost:3000');
    socket.on('newOrder', function (data) {
        getOrders();
        playSound();
    });

}]);

service.controller('shops', ['$scope', '$resource', function ($scope, $resource) {
    $scope.shops = [];
    var Shop = $resource('/api/shops/:id');
    var getShops = function () {
        Shop.get(function (res) {
            $scope.shops = res.data;
            $scope.currentEdit = {};
        });
    };
    getShops();
    $scope.startEdit = function (s) {
        if (!s) {
            $scope.modalMessage = "Добавить магазин";
        } else {
            $scope.currentEdit = s;
            $scope.modalMessage = "| редактировать";
            $scope.currentEdit.oldName = s.name;
        }
    };
    $scope.cancelEdit = function () {
        $scope.currentEdit = {};
    };
    $scope.saveService = function () {
        var nS = new Shop($scope.currentEdit);
        nS.$save({id: nS._id || ''});
        getShops();
        console.log('saved')
    };
    $scope.deleteService = function (s) {
        Shop.delete({id: s._id});
        $scope.shops.forEach(function (service, i) {
            if (s._id.toString() == service._id) $scope.shops.splice(i, 1);
        })
    };

}]);


var playSound = function () {
    var audio = new Audio('/audio/msg.ogg');
    audio.play();
};

service.controller('orders', ['$scope', '$resource', function ($scope, $resource) {
    $scope.orders = [];
    $scope.processedOrders = [];
    var Order = $resource('/api/orders/:id');
    var getProcessedOrders = function () {
        Order.get({q: "{\"processed\": true}", sort: "-processedDate"}, function (res) {
            $scope.processedOrders = res.data;
            for (var i = 0; i < $scope.processedOrders.length; i++) {
                $scope.processedOrders[i].dateFromNow = moment($scope.processedOrders[i].date).fromNow();
                $scope.processedOrders[i].date = moment($scope.processedOrders[i].date);
            }
        });
    };
    var getOrders = function () {
        Order.get({q: "{\"processed\": false}", sort: '-date'}, function (res) {
            $scope.orders = res.data;
            for (var i = 0; i < $scope.orders.length; i++) {
                $scope.orders[i].dateFromNow = moment($scope.orders[i].date).fromNow();
                $scope.orders[i].date = moment($scope.orders[i].date).format('DD.MM.YYYY HH:mm:ss');
                $scope.orders[i].time = moment($scope.orders[i].time).format('HH:mm');
            }
            console.log($scope.orders);
            getProcessedOrders();
        });
    };

    getOrders();

    $scope.setProcessed = function (o) {
        o.processed = true;
        o.processedDate = new Date();
        var order = new Order(o);
        delete order.selfExport;
        delete order.delivery;
        delete order.time;
        order.$save({id: order._id}, function (data) {
            console.log(data);
            getOrders();
        })
    };


    //var socket = io('http://sushimarket-kioto.com/'); // TODO: uncomment to production
    var socket = io('http://localhost:3000');
    socket.on('connection', function () {
        console.log("connected")
    })
    socket.on('newOrder', function (data) {
        getOrders();
        playSound();
    });


}]);

service.controller('allServices', ['$scope', '$resource', function ($scope, $resource) {


    var Product = $resource('/api/products/:id');
    var newService = false;
    var getProducts = function () {
        Product.get(function (res) {
            $scope.products = res.data;
            $scope.currentEdit = {};
        });
    };
    getProducts();
    $scope.startEdit = function (s) {
        if (!s) {
            $scope.modalMessage = "Добавить ролл";
            newService = true;
        } else {
            $scope.currentEdit = s;
            $scope.modalMessage = "| редактировать";
            newService = false;
            $scope.currentEdit.oldName = s.name;
        }
    };
    $scope.cancelEdit = function () {
        $scope.currentEdit = {};
    };
    $scope.saveService = function () {
        var nS = new Product($scope.currentEdit);
        nS.$save({id: nS._id || ''});
        getProducts();
    };
    $scope.deleteService = function (s) {
        Product.delete({id: s._id});
        $scope.products.forEach(function (service, i) {
            if (s._id.toString() == service._id) $scope.products.splice(i, 1);
        })
    };


}]);

service.controller('mainOrder', ['$scope', '$resource', function ($scope, $resource) {

    var Shop = $resource('/api/shops/:id');
    var Product = $resource('/api/products:id');
    var Order = $resource('/api/orders:id');

    $scope.shops = [];
    $scope.select = {};
    $scope.products = [];


    var getShops = function () {
        Shop.get(function (res) {
            $scope.shops = res.data;
            console.log($scope.shops);
            $scope.currentEdit = {};
            $scope.shops[0].selected = true;
            $scope.select.current = $scope.shops[0];
            $scope.order.selfExport.shop = $scope.select.current._id;
        });
    };

    var init = function () {
        $scope.order = {
            products: [],
            sum: 0,
            count: 0,
            weight: 0,
            client: {
                name: '',
                phone: ''
            },
            selfExport: {
                time: '',
                shop: ''
            },
            delivery: {},
            time: '',
            type: 'selfExport'
        };
        getShops();
    };

    init();


    $scope.selectShop = function (s) {
        s.selected = true;
        $scope.select.current = s;
        $scope.order.selfExport.shop = $scope.select.current._id;
    };


    Product.get(function (res) {
        $scope.products = res.data;
        $scope.products.forEach(function (p, i) {
            $scope.products[i].count = 1;
            $scope.products[i].composition = '';
            for (var j = 0; j < $scope.products[i].ingredients.length; j++) {
                $scope.products[i].ingredients[j].text = $scope.products[i].ingredients[j].text.replace(/---/g, ' - ');
                $scope.products[i].composition += $scope.products[i].ingredients[j].text;
                if (j != $scope.products[i].ingredients.length - 1)
                    $scope.products[i].composition += ', ';
            }

            $scope.products[i].img = $scope.products[i].name + ".jpg";
        })
    });


    $scope.saveOrder = function () {
        var nOrder = new Order({
            date: new Date(),
            client: {
                name: $scope.order.client.name,
                phone: $scope.order.client.phone
            },
            type: $scope.order.type,
            sum: $scope.order.resumeScheduled,
            basket: [],
            time: $scope.order.time
        });

        $scope.order.products.forEach(function (p) {
            nOrder.basket.push({
                product: p._id,
                count: p.count
            })
        });

        if (nOrder.type === 'selfExport') {
            nOrder.selfExport = {
                shop: $scope.order.selfExport.shop
            }
        } else if (nOrder.type === 'delivery') {
            nOrder.delivery = {
                address: {
                    house: $scope.order.delivery.address.house,
                    porch: $scope.order.delivery.address.porch,
                    floor: $scope.order.delivery.address.floor,
                    doorphone: $scope.order.delivery.address.doorphone,
                    street: $scope.order.delivery.address.street,
                    building: $scope.order.delivery.address.building,
                    padik: $scope.order.delivery.address.padik,
                    flat: $scope.order.delivery.address.flat
                }
            }
        }
        if (nOrder.selfExport || nOrder.delivery) {
            nOrder.$save({id: nOrder._id || ''}, function (res) {
                $('#orderModal').modal('hide');
                init();
                $('#spinner-01').timespinner("value", moment().format("HH:mm"));
                console.log(res);
            });
        } else {
            alert("Error. order type incorrect.")
        }

    };


    $scope.addOne = function (p) {
        p.count++;
    };
    $scope.dropOne = function (p) {
        if (p.count > 1) p.count--;
        if (!p.count) p.count = 1;
    };

    var sumCount = function (a) {
        var res = 0;
        for (var i = 0; i < a.length; i++) {
            res += a[i].count;
        }
        return res;
    };

    var sumCost = function (a) {
        var res = 0;
        for (var i = 0; i < a.length; i++) {
            res += a[i].count * a[i].cost;
        }
        return res;
    };
    var sumWeight = function (a) {
        var res = 0;
        for (var i = 0; i < a.length; i++) {
            res += a[i].count * a[i].weight;
        }
        return res;
    };

    $scope.validateCount = function (p) {
        p.count = +p.count; // toNumber
        if (p.count != '')
            if (p.count < 1 || !+p.count) p.count = 1;
    };

    $scope.addOneOrder = function (p) {
        p.count++;
        $scope.order.sum = sumCost($scope.order.products);
        $scope.order.count = sumCount($scope.order.products);
        $scope.order.weight = sumWeight($scope.order.products);
    };
    $scope.dropOneOrder = function (p) {
        if (p.count > 1) p.count--;
        if (!p.count) p.count = 1;
        $scope.order.sum = sumCost($scope.order.products);
        $scope.order.count = sumCount($scope.order.products);
        $scope.order.weight = sumWeight($scope.order.products);
    };


    $scope.setDeliveryType = function (t) {
        if ($scope.order.sum > 500)
            $scope.order.type = t;
    };

    $scope.checkSum = function () {
        if ($scope.order.sum > 500)
            $scope.order.type = 'delivery';
    };

    $scope.addToOrder = function (p) {
        var pos = -1;
        var o = {
            _id: p._id,
            name: p.name,
            count: p.count,
            cost: p.cost,
            img: p.img,
            weight: p.weight
        };
        for (var i = 0; i < $scope.order.products.length; i++) {
            if (p._id.toString() === $scope.order.products[i]._id.toString()) {
                pos = i;
                break;
            }
        }
        if (pos >= 0) {
            $scope.order.products[pos].count += p.count;
            $scope.order.sum = sumCost($scope.order.products);
            $scope.order.count = sumCount($scope.order.products);
            $scope.order.weight = sumWeight($scope.order.products);
        } else {
            $scope.order.products.push(o);
            $scope.order.sum = sumCost($scope.order.products);
            $scope.order.count = sumCount($scope.order.products);
            $scope.order.weight = sumWeight($scope.order.products);
        }
        p.count = 1;
        console.log($scope.order);

    };
    $scope.dropFromOrder = function (b) {
        var pos = -1;
        for (var i = 0; i < $scope.order.products.length; i++) {
            if (b._id.toString() === $scope.order.products[i]._id.toString()) {
                pos = i;
                break;
            }
        }
        if (pos >= 0) $scope.order.products.splice(i, 1);
    };



    // jQuery UI Spinner
    $.widget("ui.timespinner", $.ui.spinner, {
        options: {
            // seconds
            step: 15 * 60 * 1000,
            // hours
            page: 60
        },
        widgetEventPrefix: $.ui.spinner.prototype.widgetEventPrefix,
        _buttonHtml: function () { // Remove arrows on the buttons
            return "" +
                "<a class='ui-spinner-button ui-spinner-up ui-corner-tr'>" +
                "<span class='ui-icon " + this.options.icons.up + "'></span>" +
                "</a>" +
                "<a class='ui-spinner-button ui-spinner-down ui-corner-br' style='top: 22px;'>" +
                "<span class='ui-icon " + this.options.icons.down + "'></span>" +
                "</a>";
        },
        _parse: function (value) {
            if (typeof value === "string") {
                // already a timestamp
                if (Number(value) == value) {
                    return Number(value);
                }
                var res = +moment(value, "HH:mm");
                return res;
            }
            console.log("2/1"+value)
            return value;
        },
        _format: function (value) {
            $scope.order.time = value;
            var res = moment(value).format('HH:mm');
            return res;
        }
    });




}]);

