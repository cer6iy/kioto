//  kiotoman
//  54cAc56


var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('lib/logger');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var mag = require('mag');
var l = mag('app');

var mongoose = require('lib/mongoose');
var session = require('express-session');
var mongooseStore = require('express-mongoose-store')(session, mongoose);
var passport = require('passport');

require('lib/passport')(passport);

var routes = require('routes/index');
var users = require('routes/users');
var ads = require('routes/ads');
var api = require('api');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
app.use(favicon(__dirname + '/public/img/favicon (1).ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


app.use(session({
    secret: 'loadingspotmiasoiedov',
    store: new mongooseStore({ttl: 300000000}),
    cookie: {
        maxAge: 300000000
    },
    resave: false,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());


app.use('/', routes);
app.use('/users', users);
app.use('/api', api);
app.use('/ads', ads);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
/*if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        l.error('path: ' + req.path + ' error: ' + err.message);
        err.status = err.status || 500;
        res.status(err.status);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}
// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    l.error('path: ' + req.path + ' error: ' + err.message);
    err.status = err.status || 500;
    res.status(err.status);
    res.render('error', {
        message: err.message,
        error: {}
    });
});*/


module.exports = app;
