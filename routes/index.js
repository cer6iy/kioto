var express = require('express');
var router = express.Router();
var adminAccess = require('lib/passport').helpers.adminAccess;
var adminLogined = require('lib/passport').helpers.adminLogined;

/* GET home page. */
router.get('/', function (req, res) {
    res.render('index', {user: req.user});
});

router.get('/rolls', function (req, res) {
    res.render('rolls', {user: req.user});
});

router.get('/sets', function (req, res) {
    res.render('sets', {user: req.user});
});

/* GET admin pages. */
router.get('/admin', adminAccess, function (req, res) {
    res.render('admin');
});

router.get('/admin/rolls', adminAccess, function (req, res) {
    res.render('admin/rolls');
});

router.get('/admin/shops', adminAccess, function (req, res) {
    res.render('admin/shops');
});

router.get('/admin/orders', adminAccess, function (req, res) {
    res.render('admin/orders');
});

router.get('/admin/login', adminLogined, function (req, res) {
    res.render('admin/login');
});
module.exports = router;