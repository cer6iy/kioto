var express = require('express');
var router = express.Router();

/* users */
router.get('/login', function(req, res) {
  res.render('login');
});
router.get('/signup', function(req, res) {
  res.render('signUp');
});

module.exports = router;
