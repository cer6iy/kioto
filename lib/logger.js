var onFinished = require('on-finished');
var mag = require('mag');
var l = mag('query');

exports = module.exports = function () {


    return function logger(req, res, next) {
        req._startAt = Date.now();
        req._startTime = new Date;
        req._remoteAddress = getip(req);

        function logRequest() {
            var line = format(req, res);
            if (null == line) return;
            l.info(line);
        };
        onFinished(res, logRequest);
        next();

    }
}

function format(req, res){
    var d = {
        method: req.method.toUpperCase(),
        status: res._header ? res.statusCode : null,
        path: req.originalUrl || req.url,
        time: Date.now() - req._startAt,
        ip: getip(req)
    };
     return d.ip + ' ' + d.status + ' ' + d.method + ' ' + d.path + ' ' + d.time + 'ms';
}

function getip(req) {
    return req.ip
        || req._remoteAddress
        || (req.connection && req.connection.remoteAddress)
        || undefined;
}