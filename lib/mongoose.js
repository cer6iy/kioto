/*Mongoose config*/
var mongoose = require('mongoose'),
    mag = require('mag'),
    l = mag('mongoose'),
    config = require('config');
mongoose.connected = false;
mongoose.useMag = true;
var connectDB = function () {
    if (!mongoose.connected)
        mongoose.connect(config.dbURI);
}

mongoose.connection.on('connected', function () {
    mongoose.connected = true;
    var mes = "connected to: " + config.dbURI;
    if (mongoose.useMag) {l.info(mes)} else {console.log(mes)};
});

mongoose.connection.on('disconnect', function () {
    mongoose.connected = false;
    var mes = "disconnected"
    if (mongoose.useMag) {l.warn(mes)} else {console.error(mes)};
    connectDB();
});

mongoose.connection.on('error', function (err) {
    if (mongoose.useMag) {l.warn(err)} else {console.error(err)};
    connectDB();
});

connectDB();

module.exports = mongoose;