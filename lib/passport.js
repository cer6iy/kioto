// load all the things we need
var LocalStrategy = require('passport-local').Strategy;
var bcrypt = require('bcrypt-nodejs');
var mag = require('mag');
var l = mag('custom-passport');
var User = require('models/user');

module.exports = function (passport) {

    passport.serializeUser(function (user, next) {
        next(null, user.id);
    });

    passport.deserializeUser(function (id, next) {
        User.findById(id, function (err, user) {
            if (err) {
                l.error(err);
                return next(err)
            }
            next(null, user);
        });
    });

    passport.use('local-signup', new LocalStrategy({
            usernameField: 'login',
            passwordField: 'password',
            passReqToCallback: true
        },
        function (req, login, password, next) {
            User.findOne({
                    userName: login
                },
                function (err, user) {
                    if (err) {
                        l.debug(err);
                        return next(err);
                    }
                    if (user) {
                        var error = {message: 'That user name is already registered'};
                        error.status = 409;
                        l.debug(error);
                        return next(error);
                    } else {
                        var newUser = new User({
                            firstName: req.body.fName || '-',
                            lastName: req.body.lName || '-',
                            userName: login
                        });
                        newUser.local.password = newUser.generateHash(password),
                            newUser.save(function (err, u) {
                                if (err) return next(err);
                                return next(null, u);
                            });
                    }

                });

        }));

    passport.use('local-login', new LocalStrategy({
            usernameField: 'login',
            passwordField: 'password',
            passReqToCallback: true
        },
        function (req, userName, password, next) {
            User.findOne({
                    userName: userName
                },
                function (err, user) {
                    if (err) return next(err);
                    if (!user) {
                        l.debug("User not found");
                        var error = {"status": 404, "message": "User name has not been found"};
                        return next(error);
                    }

                    if (!user.validPassword(password)) {
                        var error = {"status": 409, "message": "Wrong password"};
                        return next(error);
                    }
                    next(null, user);
                });

        }));
};

module.exports.helpers = {
    adminAccess: function (req, res, next) {
        if (req.isAuthenticated() && req.user && req.user.admin)
            return next();
        if (req.method === 'GET') {
            res.redirect('/admin/login');
        }
        else {
            var err = new Error('Access Denied');
            err.status = 401;
            next(err);
        }
    },
    adminLogined: function (req, res, next) {
        if (req.isAuthenticated() && req.user && req.user.admin)
            res.redirect('/admin');
        return next();
    }
};