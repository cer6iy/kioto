/**
 * To catch all messages from mag we must
 * require 'mag-hub' before any 'mag' requires
 */

var hub = require('mag-hub');
var through2 = require('through2');
var util = require('util');
var moment = require('moment');
var config = require('config');

var env = process.env.NODE_ENV || 'local';

/**
 * Check if object o is member of data
 * @param o
 * @param {array} data
 * @return {Boolean} true if o in data
 */
var inArray = function (o, data) {
    var res = false;
    data.forEach(function (item) {
        if (o === item) res = true;
    })
    return res;
}
var severityNames = [
    'EMERG',
    'ALERT',
    'CRIT',
    'ERROR',
    'WARN',
    'NOTICE',
    'INFO',
    'DEBUG'
];

hub
    .pipe(through2.obj(function (data, enc, next) {
        var severity = severityNames[data.severity] || 'DEBUG';
        if (inArray(severity, config.log.levels)) {
            data.timestamp = moment(data.timestamp || new Date());
            /*if ((env === 'production') && (data.severity === 'DEBUG')) {
                return next(null);
            }*/
            if (!data.message) {
                if (data.arguments) {
                    data.message = util.format.apply(this, data.arguments);
                } else {
                    data.message = util.inspect(data);
                }
            }

            var severityColor = '36m';
            switch (severity) {
                case 'ERROR':
                    severityColor = '31m';
                    break;
                case 'INFO':
                    severityColor = '32m';
                    break;
                case 'WARN':
                    severityColor = '33m';
                    break;
                case 'DEBUG':
                    severityColor = '35m';
                    break;
            }
            var endl = data.message[data.message.length - 1] != '\n' ? '\n' : '';
            var str = '';
                str = '\x1b[1m' + data.timestamp.format('YYYY-MM-DD HH:mm:ss.SSS');
                if (data.namespace) {
                    str += ' \x1b[1;34m[' + data.namespace + ']\x1b[1m';
                }
                str += ' \x1b[1;' + severityColor + '<' + severity + '>\x1b[0m ' + data.message + endl;

            next(null, str);
        } else {
            next(null);
        }
    }))
    .
    pipe(process.stdout);