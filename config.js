var currentMode = process.argv[2] || process.env.NODE_ENV || "development";
var settings = {
    "development": {
        port: 3000,
        dbURI: "mongodb://localhost:27017/kyoto",
        log: {
            levels: [
                'EMERG',
                'ALERT',
                'CRIT',
                'ERROR',
                'WARN',
                'NOTICE',
                'INFO',
                'DEBUG'
            ]
        }
    },
    "production": {
        port: 3080,
        dbURI: "mongodb://localhost:30303/kyoto",
        log: {
            levels: [
                'EMERG',
                'CRIT',
                'ERROR',
                'WARN'
            ]
        }
    }
};


module.exports = settings[currentMode];